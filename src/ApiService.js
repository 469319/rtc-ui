class MockedAPI {

    static getMockProducts = async () => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(parseProductsResponse(mockData));
        }, 500);
      });
    }
  
    static getProducts = async () => {
      try {
        const response = await fetch('https://stg.rtc.afolab.cz/products');
        const data = await response.json();
        console.log(data);
        return parseProductsResponse(data);
      } catch (error) {
        console.error("Failed to fetch products:", error);
        return [];
      }
    }
  
    static searchMockProducts = async () => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(searchData);
        }, 500);
      });
    }
  };
  
  export default MockedAPI;
  
  function swapCoordinates(coordinates) {
    return coordinates.map(pair => [pair[1], pair[0]]);
  }
  
  const parseProductsResponse = (jsonObject) => {
    if (jsonObject && Array.isArray(jsonObject.products)) {
      const updatedProducts = jsonObject.products.map(product => {
        const updatedProduct = {};
          Object.keys(product).forEach(key => {
          let newKey = key;
          if (key.startsWith('X-Amz-Meta-')) {
            newKey = key.substring('X-Amz-Meta-'.length);
          }
          updatedProduct[newKey] = product[key];
        });
  
        updatedProduct.Coordinates = swapCoordinates(JSON.parse(updatedProduct.Coordinates));
        
        return updatedProduct;
      });
      return {
        ...jsonObject,
        products: updatedProducts
      };
    }
    return jsonObject;
  }
  
  const searchData = {
    "products": [
        {
            "id": 586072,
            "uuid": "e8ffe2c9-701a-4e8e-8709-64a10f4bbf9d",
            "identifier": "S1A_IW_GRDH_1SDV_20231101T051000_20231101T051025_051019_0626DB_60BF",
            "footprint": null,
            "summary": [
                "Date : 2023-11-01T05:10:00.603Z",
                "Filename : S1A_IW_GRDH_1SDV_20231101T051000_20231101T051025_051019_0626DB_60BF.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231101T051000_20231101T051025_051019_0626DB_60BF",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-11-01T05:10:00.603Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231101T051000_20231101T051025_051019_0626DB_60BF.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231101T051000_20231101T051025_051019_0626DB_60BF",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "306",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.060902,17.202078 49.471054,13.607963 50.967068,13.964918 50.555328,17.671970 49.060902,17.202078</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-11-01T06:11:55.195Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((17.202078 49.060902, 17.67197 50.555328, 13.964918 50.967068, 13.607963 49.471054, 17.202078 49.060902)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "403163",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "51019",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "51019",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-11-01T05:10:00.603Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-11-01T05:10:25.601Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "17",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((17.202078 49.060902, 17.67197 50.555328, 13.964918 50.967068, 13.607963 49.471054, 17.202078 49.060902)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 586071,
            "uuid": "c52487e1-fee6-47d2-96e4-a67669983152",
            "identifier": "S1A_IW_GRDH_1SDV_20231101T051025_20231101T051050_051019_0626DB_DB40",
            "footprint": null,
            "summary": [
                "Date : 2023-11-01T05:10:25.603Z",
                "Filename : S1A_IW_GRDH_1SDV_20231101T051025_20231101T051050_051019_0626DB_DB40.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231101T051025_20231101T051050_051019_0626DB_DB40",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-11-01T05:10:25.603Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231101T051025_20231101T051050_051019_0626DB_DB40.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231101T051025_20231101T051050_051019_0626DB_DB40",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "306",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>47.566200,16.741768 47.974930,13.253104 49.470833,13.609308 49.060810,17.202053 47.566200,16.741768</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-11-01T06:09:17.716Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((16.741768 47.5662, 17.202053 49.06081, 13.609308 49.470833, 13.253104 47.97493, 16.741768 47.5662)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "403163",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "51019",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "51019",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-11-01T05:10:25.603Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-11-01T05:10:50.601Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "18",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((16.741768 47.5662, 17.202053 49.06081, 13.609308 49.470833, 13.253104 47.97493, 16.741768 47.5662)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 586070,
            "uuid": "12e67d75-ff25-4cad-874d-a68ed5745bf7",
            "identifier": "S1A_IW_GRDH_1SDV_20231101T050935_20231101T051000_051019_0626DB_E038",
            "footprint": null,
            "summary": [
                "Date : 2023-11-01T05:09:35.603Z",
                "Filename : S1A_IW_GRDH_1SDV_20231101T050935_20231101T051000_051019_0626DB_E038.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231101T050935_20231101T051000_051019_0626DB_E038",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-11-01T05:09:35.603Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231101T050935_20231101T051000_051019_0626DB_E038.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231101T050935_20231101T051000_051019_0626DB_E038",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "306",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "22",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>50.555416,17.671999 50.967304,13.963389 52.462025,14.336735 52.047714,18.168056 50.555416,17.671999</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-11-01T06:08:56.219Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((17.671999 50.555416, 18.168056 52.047714, 14.336735 52.462025, 13.963389 50.967304, 17.671999 50.555416)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "403163",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "51019",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "51019",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-11-01T05:09:35.603Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-11-01T05:10:00.601Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "16",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((17.671999 50.555416, 18.168056 52.047714, 14.336735 52.462025, 13.963389 50.967304, 17.671999 50.555416)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585971,
            "uuid": "e45f3b54-aa1b-4f68-a5e0-32991966f0f2",
            "identifier": "S1A_IW_GRDH_1SDV_20231030T163544_20231030T163609_050997_062607_7B48",
            "footprint": null,
            "summary": [
                "Date : 2023-10-30T16:35:44.305Z",
                "Filename : S1A_IW_GRDH_1SDV_20231030T163544_20231030T163609_050997_062607_7B48.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231030T163544_20231030T163609_050997_062607_7B48",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-30T16:35:44.305Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231030T163544_20231030T163609_050997_062607_7B48.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231030T163544_20231030T163609_050997_062607_7B48",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>51.280617,16.345760 51.692978,20.105804 50.197815,20.473677 49.787537,16.830988 51.280617,16.345760</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-30T17:37:10.825Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((16.830988 49.787537, 20.473677 50.197815, 20.105804 51.692978, 16.34576 51.280617, 16.830988 49.787537)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402951",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50997",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50997",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-30T16:35:44.305Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-30T16:36:09.304Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "14",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((16.830988 49.787537, 20.473677 50.197815, 20.105804 51.692978, 16.34576 51.280617, 16.830988 49.787537)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585970,
            "uuid": "fe98b79e-b9cc-466f-94c5-0d26f2746a39",
            "identifier": "S1A_IW_GRDH_1SDV_20231030T163519_20231030T163544_050997_062607_F9A6",
            "footprint": null,
            "summary": [
                "Date : 2023-10-30T16:35:19.305Z",
                "Filename : S1A_IW_GRDH_1SDV_20231030T163519_20231030T163544_050997_062607_F9A6.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231030T163519_20231030T163544_050997_062607_F9A6",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-30T16:35:19.305Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231030T163519_20231030T163544_050997_062607_F9A6.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231030T163519_20231030T163544_050997_062607_F9A6",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "175",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.787445,16.831017 50.197590,20.472313 48.700783,20.820915 48.291786,17.287575 49.787445,16.831017</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-30T17:33:15.442Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((17.287575 48.291786, 20.820915 48.700783, 20.472313 50.19759, 16.831017 49.787445, 17.287575 48.291786)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402951",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50997",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50997",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-30T16:35:19.305Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-30T16:35:44.303Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "13",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((17.287575 48.291786, 20.820915 48.700783, 20.472313 50.19759, 16.831017 49.787445, 17.287575 48.291786)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585950,
            "uuid": "c6b136aa-9d3e-45a8-a039-15118977eef1",
            "identifier": "S1A_IW_GRDH_1SDV_20231030T052556_20231030T052621_050990_0625CB_D08D",
            "footprint": null,
            "summary": [
                "Date : 2023-10-30T05:25:56.541Z",
                "Filename : S1A_IW_GRDH_1SDV_20231030T052556_20231030T052621_050990_0625CB_D08D.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231030T052556_20231030T052621_050990_0625CB_D08D",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-30T05:25:56.541Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231030T052556_20231030T052621_050990_0625CB_D08D.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231030T052556_20231030T052621_050990_0625CB_D08D",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>50.962498,13.686982 51.374653,9.948289 52.869286,10.323249 52.454575,14.187415 50.962498,13.686982</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-30T06:52:35.126Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((13.686982 50.962498, 14.187415 52.454575, 10.323249 52.869286, 9.948289 51.374653, 13.686982 50.962498)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402891",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50990",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50990",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-30T05:25:56.541Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-30T05:26:21.540Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "16",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((13.686982 50.962498, 14.187415 52.454575, 10.323249 52.869286, 9.948289 51.374653, 13.686982 50.962498)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585949,
            "uuid": "0ae4c7b6-cb87-48f3-a8cb-bee7d0f1759d",
            "identifier": "S1A_IW_GRDH_1SDV_20231030T052621_20231030T052646_050990_0625CB_5446",
            "footprint": null,
            "summary": [
                "Date : 2023-10-30T05:26:21.541Z",
                "Filename : S1A_IW_GRDH_1SDV_20231030T052621_20231030T052646_050990_0625CB_5446.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231030T052621_20231030T052646_050990_0625CB_5446",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-30T05:26:21.541Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231030T052621_20231030T052646_050990_0625CB_5446.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231030T052621_20231030T052646_050990_0625CB_5446",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.469101,13.206161 49.879169,9.584472 51.374420,9.949829 50.962410,13.686953 49.469101,13.206161</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-30T06:50:46.805Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((13.206161 49.469101, 13.686953 50.96241, 9.949829 51.37442, 9.584472 49.879169, 13.206161 49.469101)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402891",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50990",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50990",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-30T05:26:21.541Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-30T05:26:46.540Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "17",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((13.206161 49.469101, 13.686953 50.96241, 9.949829 51.37442, 9.584472 49.879169, 13.206161 49.469101)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585948,
            "uuid": "13c088ca-a054-4317-8601-d7fb6c560897",
            "identifier": "S1A_IW_GRDH_1SDV_20231030T052646_20231030T052711_050990_0625CB_34A9",
            "footprint": null,
            "summary": [
                "Date : 2023-10-30T05:26:46.541Z",
                "Filename : S1A_IW_GRDH_1SDV_20231030T052646_20231030T052711_050990_0625CB_34A9.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231030T052646_20231030T052711_050990_0625CB_34A9",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-30T05:26:46.541Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231030T052646_20231030T052711_050990_0625CB_34A9.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231030T052646_20231030T052711_050990_0625CB_34A9",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "168",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>47.973713,12.749186 48.382465,9.235285 49.878933,9.585962 49.469009,13.206130 47.973713,12.749186</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-30T06:50:44.291Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((12.749186 47.973713, 13.20613 49.469009, 9.585962 49.878933, 9.235285 48.382465, 12.749186 47.973713)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402891",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50990",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50990",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-30T05:26:46.541Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-30T05:27:11.539Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "18",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((12.749186 47.973713, 13.20613 49.469009, 9.585962 49.878933, 9.235285 48.382465, 12.749186 47.973713)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585920,
            "uuid": "44871783-626c-41f3-a0e7-6cc0ab388d59",
            "identifier": "S1A_IW_GRDH_1SDV_20231028T165149_20231028T165214_050968_062503_3978",
            "footprint": null,
            "summary": [
                "Date : 2023-10-28T16:51:49.598Z",
                "Filename : S1A_IW_GRDH_1SDV_20231028T165149_20231028T165214_050968_062503_3978.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231028T165149_20231028T165214_050968_062503_3978",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.62 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.62 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-28T16:51:49.598Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231028T165149_20231028T165214_050968_062503_3978.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231028T165149_20231028T165214_050968_062503_3978",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.947712,12.665695 50.350986,16.245663 48.854576,16.601664 48.452686,13.128445 49.947712,12.665695</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-28T19:20:31.495Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((13.128445 48.452686, 16.601664 48.854576, 16.245663 50.350986, 12.665695 49.947712, 13.128445 48.452686)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402691",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50968",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50968",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-28T16:51:49.598Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-28T16:52:14.597Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "0",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((13.128445 48.452686, 16.601664 48.854576, 16.245663 50.350986, 12.665695 49.947712, 13.128445 48.452686)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585921,
            "uuid": "b2d5b478-b1f4-48e5-9b71-03a8e1e664fe",
            "identifier": "S1A_IW_GRDH_1SDV_20231028T165124_20231028T165149_050968_062503_8D6A",
            "footprint": null,
            "summary": [
                "Date : 2023-10-28T16:51:24.598Z",
                "Filename : S1A_IW_GRDH_1SDV_20231028T165124_20231028T165149_050968_062503_8D6A.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231028T165124_20231028T165149_050968_062503_8D6A",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.62 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.62 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-28T16:51:24.598Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231028T165124_20231028T165149_050968_062503_8D6A.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231028T165124_20231028T165149_050968_062503_8D6A",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>48.452595,13.128470 48.854351,16.600338 47.359119,16.965212 46.958809,13.592073 48.452595,13.128470</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-28T19:20:30.729Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((13.592073 46.958809, 16.965212 47.359119, 16.600338 48.854351, 13.12847 48.452595, 13.592073 46.958809)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402691",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50968",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50968",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-28T16:51:24.598Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-28T16:51:49.596Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "0",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((13.592073 46.958809, 16.965212 47.359119, 16.600338 48.854351, 13.12847 48.452595, 13.592073 46.958809)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585922,
            "uuid": "3e855112-b7f6-4ab3-85a7-12a43d3eab2c",
            "identifier": "S1A_IW_GRDH_1SDV_20231028T165214_20231028T165239_050968_062503_2B21",
            "footprint": null,
            "summary": [
                "Date : 2023-10-28T16:52:14.599Z",
                "Filename : S1A_IW_GRDH_1SDV_20231028T165214_20231028T165239_050968_062503_2B21.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231028T165214_20231028T165239_050968_062503_2B21",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.62 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.62 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-28T16:52:14.599Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231028T165214_20231028T165239_050968_062503_2B21.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231028T165214_20231028T165239_050968_062503_2B21",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "146",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>51.440197,12.174200 51.846062,15.873169 50.351357,16.248558 49.947800,12.665667 51.440197,12.174200</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-28T19:18:23.611Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((12.665667 49.9478, 16.248558 50.351357, 15.873169 51.846062, 12.1742 51.440197, 12.665667 49.9478)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402691",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50968",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50968",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-28T16:52:14.599Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-28T16:52:39.597Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "0",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((12.665667 49.9478, 16.248558 50.351357, 15.873169 51.846062, 12.1742 51.440197, 12.665667 49.9478)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585836,
            "uuid": "ee89d4d6-a934-4a63-b590-e4791626c749",
            "identifier": "S1A_IW_GRDH_1SDV_20231027T050153_20231027T050218_050946_062446_5D8C",
            "footprint": null,
            "summary": [
                "Date : 2023-10-27T05:01:53.819Z",
                "Filename : S1A_IW_GRDH_1SDV_20231027T050153_20231027T050218_050946_062446_5D8C.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231027T050153_20231027T050218_050946_062446_5D8C",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-27T05:01:53.819Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231027T050153_20231027T050218_050946_062446_5D8C.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231027T050153_20231027T050218_050946_062446_5D8C",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>48.672695,19.132530 49.081757,15.571817 50.577431,15.931231 50.166779,19.602322 48.672695,19.132530</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-27T06:09:53.542Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((19.13253 48.672695, 19.602322 50.166779, 15.931231 50.577431, 15.571817 49.081757, 19.13253 48.672695)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402502",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50946",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50946",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-27T05:01:53.819Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-27T05:02:18.817Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "17",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((19.13253 48.672695, 19.602322 50.166779, 15.931231 50.577431, 15.571817 49.081757, 19.13253 48.672695)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585837,
            "uuid": "33741455-760a-4084-a7a8-f32c88a4eae4",
            "identifier": "S1A_IW_GRDH_1SDV_20231027T050128_20231027T050153_050946_062446_D9EC",
            "footprint": null,
            "summary": [
                "Date : 2023-10-27T05:01:28.819Z",
                "Filename : S1A_IW_GRDH_1SDV_20231027T050128_20231027T050153_050946_062446_D9EC.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231027T050128_20231027T050153_050946_062446_D9EC",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-27T05:01:28.819Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231027T050128_20231027T050153_050946_062446_D9EC.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231027T050128_20231027T050153_050946_062446_D9EC",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>50.166866,19.602350 50.577667,15.929717 52.073002,16.296085 51.660099,20.088614 50.166866,19.602350</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-27T06:09:46.305Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((19.60235 50.166866, 20.088614 51.660099, 16.296085 52.073002, 15.929717 50.577667, 19.60235 50.166866)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402502",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50946",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50946",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-27T05:01:28.819Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-27T05:01:53.818Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "16",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((19.60235 50.166866, 20.088614 51.660099, 16.296085 52.073002, 15.929717 50.577667, 19.60235 50.166866)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585838,
            "uuid": "de961d8b-9553-4388-bf8a-5c1095152bf8",
            "identifier": "S1A_IW_GRDH_1SDV_20231027T050218_20231027T050243_050946_062446_ECC9",
            "footprint": null,
            "summary": [
                "Date : 2023-10-27T05:02:18.818Z",
                "Filename : S1A_IW_GRDH_1SDV_20231027T050218_20231027T050243_050946_062446_ECC9.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231027T050218_20231027T050243_050946_062446_ECC9",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-27T05:02:18.818Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231027T050218_20231027T050243_050946_062446_ECC9.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231027T050218_20231027T050243_050946_062446_ECC9",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "124",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>47.175766,18.690783 47.584015,15.233081 49.081535,15.573151 48.672607,19.132505 47.175766,18.690783</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-27T06:07:01.730Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((18.690783 47.175766, 19.132505 48.672607, 15.573151 49.081535, 15.233081 47.584015, 18.690783 47.175766)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402502",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50946",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50946",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-27T05:02:18.818Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-27T05:02:43.817Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "18",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((18.690783 47.175766, 19.132505 48.672607, 15.573151 49.081535, 15.233081 47.584015, 18.690783 47.175766)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585154,
            "uuid": "111f0ad1-c122-46fb-b923-b5b10262fd88",
            "identifier": "S1A_IW_GRDH_1SDV_20231025T162707_20231025T162732_050924_06238D_3C59",
            "footprint": null,
            "summary": [
                "Date : 2023-10-25T16:27:07.689Z",
                "Filename : S1A_IW_GRDH_1SDV_20231025T162707_20231025T162732_050924_06238D_3C59.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231025T162707_20231025T162732_050924_06238D_3C59",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-25T16:27:07.689Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231025T162707_20231025T162732_050924_06238D_3C59.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231025T162707_20231025T162732_050924_06238D_3C59",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.908009,18.846581 50.318535,22.498102 48.822479,22.854570 48.413395,19.311831 49.908009,18.846581</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-25T17:20:45.199Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((19.311831 48.413395, 22.85457 48.822479, 22.498102 50.318535, 18.846581 49.908009, 19.311831 48.413395)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402317",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50924",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50924",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-25T16:27:07.689Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-25T16:27:32.688Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "13",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((19.311831 48.413395, 22.85457 48.822479, 22.498102 50.318535, 18.846581 49.908009, 19.311831 48.413395)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585153,
            "uuid": "21a2d52e-7fe6-476d-9595-2529d0e7ea30",
            "identifier": "S1A_IW_GRDH_1SDV_20231025T162732_20231025T162757_050924_06238D_DF28",
            "footprint": null,
            "summary": [
                "Date : 2023-10-25T16:27:32.689Z",
                "Filename : S1A_IW_GRDH_1SDV_20231025T162732_20231025T162757_050924_06238D_DF28.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231025T162732_20231025T162757_050924_06238D_DF28",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-25T16:27:32.689Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231025T162732_20231025T162757_050924_06238D_DF28.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231025T162732_20231025T162757_050924_06238D_DF28",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "102",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>51.401596,18.363766 51.814400,22.136461 50.318905,22.500992 49.908100,18.846546 51.401596,18.363766</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-25T17:20:43.723Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((18.846546 49.9081, 22.500992 50.318905, 22.136461 51.8144, 18.363766 51.401596, 18.846546 49.9081)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402317",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50924",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50924",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-25T16:27:32.689Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-25T16:27:57.688Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "14",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((18.846546 49.9081, 22.500992 50.318905, 22.136461 51.8144, 18.363766 51.401596, 18.846546 49.9081)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585039,
            "uuid": "e0c322fa-69b1-453f-9867-ad7295412e2c",
            "identifier": "S1A_IW_GRDH_1SDV_20231025T051812_20231025T051837_050917_06234A_7FA1",
            "footprint": null,
            "summary": [
                "Date : 2023-10-25T05:18:12.699Z",
                "Filename : S1A_IW_GRDH_1SDV_20231025T051812_20231025T051837_050917_06234A_7FA1.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231025T051812_20231025T051837_050917_06234A_7FA1",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-25T05:18:12.699Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231025T051812_20231025T051837_050917_06234A_7FA1.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231025T051812_20231025T051837_050917_06234A_7FA1",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.182255,15.180235 49.592709,11.575044 51.088730,11.932267 50.676666,15.651164 49.182255,15.180235</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-25T06:23:07.391Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((15.180235 49.182255, 15.651164 50.676666, 11.932267 51.08873, 11.575044 49.592709, 15.180235 49.182255)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402250",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50917",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50917",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-25T05:18:12.699Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-25T05:18:37.698Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "17",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((15.180235 49.182255, 15.651164 50.676666, 11.932267 51.08873, 11.575044 49.592709, 15.180235 49.182255)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585038,
            "uuid": "0054897e-6279-4491-ae3e-c9f96881b43d",
            "identifier": "S1A_IW_GRDH_1SDV_20231025T051747_20231025T051812_050917_06234A_30F7",
            "footprint": null,
            "summary": [
                "Date : 2023-10-25T05:17:47.700Z",
                "Filename : S1A_IW_GRDH_1SDV_20231025T051747_20231025T051812_050917_06234A_30F7.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231025T051747_20231025T051812_050917_06234A_30F7",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-25T05:17:47.700Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231025T051747_20231025T051812_050917_06234A_30F7.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231025T051747_20231025T051812_050917_06234A_30F7",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>50.676754,15.651190 51.088951,11.930875 52.583252,12.308287 52.168453,16.152311 50.676754,15.651190</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-25T06:22:56.190Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((15.65119 50.676754, 16.152311 52.168453, 12.308287 52.583252, 11.930875 51.088951, 15.65119 50.676754)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402250",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50917",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50917",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-25T05:17:47.700Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-25T05:18:12.697Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "16",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((15.65119 50.676754, 16.152311 52.168453, 12.308287 52.583252, 11.930875 51.088951, 15.65119 50.676754)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 585037,
            "uuid": "2e0a5710-9352-4759-9bae-b79c1d80c51c",
            "identifier": "S1A_IW_GRDH_1SDV_20231025T051837_20231025T051902_050917_06234A_E922",
            "footprint": null,
            "summary": [
                "Date : 2023-10-25T05:18:37.699Z",
                "Filename : S1A_IW_GRDH_1SDV_20231025T051837_20231025T051902_050917_06234A_E922.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231025T051837_20231025T051902_050917_06234A_E922",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-25T05:18:37.699Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231025T051837_20231025T051902_050917_06234A_E922.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231025T051837_20231025T051902_050917_06234A_E922",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "95",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>47.687557,14.719063 48.096577,11.219949 49.592487,11.576391 49.182167,15.180208 47.687557,14.719063</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-25T06:19:48.380Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((14.719063 47.687557, 15.180208 49.182167, 11.576391 49.592487, 11.219949 48.096577, 14.719063 47.687557)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402250",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50917",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50917",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-25T05:18:37.699Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-25T05:19:02.698Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "18",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((14.719063 47.687557, 15.180208 49.182167, 11.576391 49.592487, 11.219949 48.096577, 14.719063 47.687557)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 584844,
            "uuid": "8a9d31e4-1be7-4aec-afde-5dd92ba17d15",
            "identifier": "S1A_IW_GRDH_1SDV_20231023T164411_20231023T164436_050895_062286_E75D",
            "footprint": null,
            "summary": [
                "Date : 2023-10-23T16:44:11.323Z",
                "Filename : S1A_IW_GRDH_1SDV_20231023T164411_20231023T164436_050895_062286_E75D.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231023T164411_20231023T164436_050895_062286_E75D",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-23T16:44:11.323Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231023T164411_20231023T164436_050895_062286_E75D.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231023T164411_20231023T164436_050895_062286_E75D",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>52.054951,14.023082 52.469448,17.854666 50.975010,18.231670 50.563053,14.522777 52.054951,14.023082</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-23T17:46:38.261Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((14.522777 50.563053, 18.23167 50.97501, 17.854666 52.469448, 14.023082 52.054951, 14.522777 50.563053)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402054",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50895",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50895",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-23T16:44:11.323Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-23T16:44:36.321Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "15",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((14.522777 50.563053, 18.23167 50.97501, 17.854666 52.469448, 14.023082 52.054951, 14.522777 50.563053)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 584842,
            "uuid": "54e91de5-04c6-4700-a5c8-8d0857afb77b",
            "identifier": "S1A_IW_GRDH_1SDV_20231023T164346_20231023T164411_050895_062286_4888",
            "footprint": null,
            "summary": [
                "Date : 2023-10-23T16:43:46.323Z",
                "Filename : S1A_IW_GRDH_1SDV_20231023T164346_20231023T164411_050895_062286_4888.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231023T164346_20231023T164411_050895_062286_4888",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-23T16:43:46.323Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231023T164346_20231023T164411_050895_062286_4888.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231023T164346_20231023T164411_050895_062286_4888",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>50.562965,14.522808 50.974773,18.230146 49.478806,18.588123 49.068626,14.993825 50.562965,14.522808</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-23T17:44:40.272Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((14.993825 49.068626, 18.588123 49.478806, 18.230146 50.974773, 14.522808 50.562965, 14.993825 49.068626)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402054",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50895",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50895",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-23T16:43:46.323Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-23T16:44:11.321Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "14",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((14.993825 49.068626, 18.588123 49.478806, 18.230146 50.974773, 14.522808 50.562965, 14.993825 49.068626)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 584843,
            "uuid": "12e230db-3f38-4e69-bc15-91b98be122ba",
            "identifier": "S1A_IW_GRDH_1SDV_20231023T164321_20231023T164346_050895_062286_DDB2",
            "footprint": null,
            "summary": [
                "Date : 2023-10-23T16:43:21.323Z",
                "Filename : S1A_IW_GRDH_1SDV_20231023T164321_20231023T164346_050895_062286_DDB2.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231023T164321_20231023T164346_050895_062286_DDB2",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-23T16:43:21.323Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231023T164321_20231023T164346_050895_062286_DDB2.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231023T164321_20231023T164346_050895_062286_DDB2",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "73",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.068539,14.993852 49.478584,18.586779 47.982216,18.938841 47.573338,15.450114 49.068539,14.993852</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-23T17:44:39.924Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((15.450114 47.573338, 18.938841 47.982216, 18.586779 49.478584, 14.993852 49.068539, 15.450114 47.573338)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "402054",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50895",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50895",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-23T16:43:21.323Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-23T16:43:46.321Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "13",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((15.450114 47.573338, 18.938841 47.982216, 18.586779 49.478584, 14.993852 49.068539, 15.450114 47.573338)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 584580,
            "uuid": "a795213f-cacd-4b1f-9f4e-fd81a91e0a93",
            "identifier": "S1A_IW_GRDH_1SDV_20231022T045407_20231022T045432_050873_0621B7_28CE",
            "footprint": null,
            "summary": [
                "Date : 2023-10-22T04:54:07.385Z",
                "Filename : S1A_IW_GRDH_1SDV_20231022T045407_20231022T045432_050873_0621B7_28CE.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231022T045407_20231022T045432_050873_0621B7_28CE",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-22T04:54:07.385Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231022T045407_20231022T045432_050873_0621B7_28CE.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231022T045407_20231022T045432_050873_0621B7_28CE",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>47.053535,20.718872 47.462368,17.265705 48.960247,17.602386 48.550869,21.156544 47.053535,20.718872</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-22T05:55:20.654Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((20.718872 47.053535, 21.156544 48.550869, 17.602386 48.960247, 17.265705 47.462368, 20.718872 47.053535)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "401847",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50873",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50873",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-22T04:54:07.385Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-22T04:54:32.384Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "18",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((20.718872 47.053535, 21.156544 48.550869, 17.602386 48.960247, 17.265705 47.462368, 20.718872 47.053535)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 584579,
            "uuid": "02fb5f1f-0eab-4386-ae9d-312f33d95b33",
            "identifier": "S1A_IW_GRDH_1SDV_20231022T045342_20231022T045407_050873_0621B7_9F3D",
            "footprint": null,
            "summary": [
                "Date : 2023-10-22T04:53:42.384Z",
                "Filename : S1A_IW_GRDH_1SDV_20231022T045342_20231022T045407_050873_0621B7_9F3D.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231022T045342_20231022T045407_050873_0621B7_9F3D",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-22T04:53:42.384Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231022T045342_20231022T045407_050873_0621B7_9F3D.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231022T045342_20231022T045407_050873_0621B7_9F3D",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "DESCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "51",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>48.550957,21.156569 48.960468,17.601055 50.456234,17.960539 50.045135,21.625889 48.550957,21.156569</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-22T05:53:33.602Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((21.156569 48.550957, 21.625889 50.045135, 17.960539 50.456234, 17.601055 48.960468, 21.156569 48.550957)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "401847",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50873",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50873",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-22T04:53:42.384Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-22T04:54:07.383Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "17",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((21.156569 48.550957, 21.625889 50.045135, 17.960539 50.456234, 17.601055 48.960468, 21.156569 48.550957)))",
            "offline": false,
            "order": null,
            "transformation": null
        },
        {
            "id": 584522,
            "uuid": "04070ddc-c28e-4286-9665-67db88808cbb",
            "identifier": "S1A_IW_GRDH_1SDV_20231021T165959_20231021T170024_050866_062187_5873",
            "footprint": null,
            "summary": [
                "Date : 2023-10-21T16:59:59.624Z",
                "Filename : S1A_IW_GRDH_1SDV_20231021T165959_20231021T170024_050866_062187_5873.SAFE",
                "Identifier : S1A_IW_GRDH_1SDV_20231021T165959_20231021T170024_050866_062187_5873",
                "Instrument : SAR-C",
                "Mode : IW",
                "Satellite : Sentinel-1",
                "Size : 1.65 GB"
            ],
            "indexes": [
                {
                    "name": "summary",
                    "value": null,
                    "children": [
                        {
                            "name": "Size",
                            "value": "1.65 GB",
                            "children": null
                        },
                        {
                            "name": "Instrument",
                            "value": "SAR-C",
                            "children": null
                        },
                        {
                            "name": "Mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Satellite",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Date",
                            "value": "2023-10-21T16:59:59.624Z",
                            "children": null
                        },
                        {
                            "name": "Filename",
                            "value": "S1A_IW_GRDH_1SDV_20231021T165959_20231021T170024_050866_062187_5873.SAFE",
                            "children": null
                        },
                        {
                            "name": "Identifier",
                            "value": "S1A_IW_GRDH_1SDV_20231021T165959_20231021T170024_050866_062187_5873",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "product",
                    "value": null,
                    "children": [
                        {
                            "name": "Stop relative orbit number",
                            "value": "44",
                            "children": null
                        },
                        {
                            "name": "Product composition",
                            "value": "Slice",
                            "children": null
                        },
                        {
                            "name": "Status",
                            "value": "ARCHIVED",
                            "children": null
                        },
                        {
                            "name": "Phase identifier",
                            "value": "1",
                            "children": null
                        },
                        {
                            "name": "Start relative orbit number",
                            "value": "44",
                            "children": null
                        },
                        {
                            "name": "Cycle number",
                            "value": "305",
                            "children": null
                        },
                        {
                            "name": "Product class description",
                            "value": "SAR Standard L1 Product",
                            "children": null
                        },
                        {
                            "name": "Format",
                            "value": "SAFE",
                            "children": null
                        },
                        {
                            "name": "Product level",
                            "value": "L1",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (start)",
                            "value": "44",
                            "children": null
                        },
                        {
                            "name": "Timeliness Category",
                            "value": "NRT-3h",
                            "children": null
                        },
                        {
                            "name": "Product type",
                            "value": "GRD",
                            "children": null
                        },
                        {
                            "name": "Pass direction",
                            "value": "ASCENDING",
                            "children": null
                        },
                        {
                            "name": "Relative orbit (stop)",
                            "value": "44",
                            "children": null
                        },
                        {
                            "name": "Acquisition Type",
                            "value": "NOMINAL",
                            "children": null
                        },
                        {
                            "name": "Footprint",
                            "value": "<gml:Polygon srsName=\"http://www.opengis.net/gml/srs/epsg.xml#4326\" xmlns:gml=\"http://www.opengis.net/gml\">\n   <gml:outerBoundaryIs>\n      <gml:LinearRing>\n         <gml:coordinates>49.713364,10.680635 50.123093,14.312041 48.626801,14.665351 48.218369,11.141429 49.713364,10.680635</gml:coordinates>\n      </gml:LinearRing>\n   </gml:outerBoundaryIs>\n</gml:Polygon>",
                            "children": null
                        },
                        {
                            "name": "Ingestion Date",
                            "value": "2023-10-21T17:56:26.866Z",
                            "children": null
                        },
                        {
                            "name": "JTS footprint",
                            "value": "MULTIPOLYGON (((11.141429 48.218369, 14.665351 48.626801, 14.312041 50.123093, 10.680635 49.713364, 11.141429 48.218369)))",
                            "children": null
                        },
                        {
                            "name": "Mission datatake id",
                            "value": "401799",
                            "children": null
                        },
                        {
                            "name": "Orbit number (start)",
                            "value": "50866",
                            "children": null
                        },
                        {
                            "name": "Orbit number (stop)",
                            "value": "50866",
                            "children": null
                        },
                        {
                            "name": "Polarisation",
                            "value": "VV VH",
                            "children": null
                        },
                        {
                            "name": "Product class",
                            "value": "S",
                            "children": null
                        },
                        {
                            "name": "Sensing start",
                            "value": "2023-10-21T16:59:59.624Z",
                            "children": null
                        },
                        {
                            "name": "Sensing stop",
                            "value": "2023-10-21T17:00:24.621Z",
                            "children": null
                        },
                        {
                            "name": "Slice number",
                            "value": "13",
                            "children": null
                        },
                        {
                            "name": "Resolution",
                            "value": "High",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument description text",
                            "value": "The SAR Antenna Subsystem (SAS) is developed and build by AstriumGmbH. It is a large foldable planar phased array antenna, which isformed by a centre panel and two antenna side wings. In deployedconfiguration the antenna has an overall aperture of 12.3 x 0.84 m.The antenna provides a fast electronic scanning capability inazimuth and elevation and is based on low loss and highly stablewaveguide radiators build in carbon fibre technology, which arealready successfully used by the TerraSAR-X radar imaging mission.The SAR Electronic Subsystem (SES) is developed and build byAstrium Ltd. It provides all radar control, IF/ RF signalgeneration and receive data handling functions for the SARInstrument. The fully redundant SES is based on a channelisedarchitecture with one transmit and two receive chains, providing amodular approach to the generation and reception of wide-bandsignals and the handling of multi-polarisation modes. One keyfeature is the implementation of the Flexible Dynamic BlockAdaptive Quantisation (FD-BAQ) data compression concept, whichallows an efficient use of on-board storage resources and minimisesdownlink times.",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "platform",
                    "value": null,
                    "children": [
                        {
                            "name": "Carrier rocket",
                            "value": "Soyuz",
                            "children": null
                        },
                        {
                            "name": "Satellite number",
                            "value": "A",
                            "children": null
                        },
                        {
                            "name": "Launch date",
                            "value": "April 3rd, 2014",
                            "children": null
                        },
                        {
                            "name": "Mission type",
                            "value": "Earth observation",
                            "children": null
                        },
                        {
                            "name": "NSSDC identifier",
                            "value": "2014-016A",
                            "children": null
                        },
                        {
                            "name": "Operator",
                            "value": "European Space Agency",
                            "children": null
                        },
                        {
                            "name": "Satellite name",
                            "value": "Sentinel-1",
                            "children": null
                        },
                        {
                            "name": "Satellite description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        }
                    ]
                },
                {
                    "name": "instrument",
                    "value": null,
                    "children": [
                        {
                            "name": "Instrument abbreviation",
                            "value": "SAR-C SAR",
                            "children": null
                        },
                        {
                            "name": "Instrument name",
                            "value": "Synthetic Aperture Radar (C-band)",
                            "children": null
                        },
                        {
                            "name": "Instrument mode",
                            "value": "IW",
                            "children": null
                        },
                        {
                            "name": "Instrument description",
                            "value": "<a target=\"_blank\" href=\"https://sentinel.esa.int/web/sentinel/missions/sentinel-1\">https://sentinel.esa.int/web/sentinel/missions/sentinel-1</a>",
                            "children": null
                        },
                        {
                            "name": "Instrument swath",
                            "value": "IW",
                            "children": null
                        }
                    ]
                }
            ],
            "thumbnail": true,
            "quicklook": true,
            "instrument": "SAR-C",
            "productType": "GRD",
            "itemClass": "http://www.esa.int/s1#IWlevel1sS1Aproduct",
            "wkt": "MULTIPOLYGON (((11.141429 48.218369, 14.665351 48.626801, 14.312041 50.123093, 10.680635 49.713364, 11.141429 48.218369)))",
            "offline": false,
            "order": null,
            "transformation": null
        }
    ],
    "totalresults": 77,
    "totalproducts": 0
  };
  
  const mockData = {
      "products": [
        {
          "X-Amz-Meta-Status": "Queued",
          "X-Amz-Meta-Format": "SAFE",
          "X-Amz-Meta-Uuid": "ace2e689-b79e-4526-a4e3-7ec24b819e82",
          "X-Amz-Meta-Coordinates": "[[21.157568, 48.551117], [21.62689, 50.045296], [17.961241, 50.456383], [17.601765, 48.960617], [21.157568, 48.551117]]",
          "X-Amz-Meta-Polarisation": "VV VH",
          "X-Amz-Meta-Productlevel": "L1",
          "content-type": "application/octet-stream",
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231103T045341_20231103T045406_051048_0627BA_E682",
          "X-Amz-Meta-Producttype": "GRD",
          "X-Amz-Meta-Mode": "IW"
        },
        {
          "X-Amz-Meta-Format": "SAFE",
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Status": "Queued",
          "content-type": "application/octet-stream",
          "X-Amz-Meta-Uuid": "4d9ef240-f2d9-4147-99de-dfbbd928a6f2",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231104T164320_20231104T164345_051070_062883_8BC7",
          "X-Amz-Meta-Productlevel": "L1",
          "X-Amz-Meta-Coordinates": "[[15.450293, 47.573086], [18.939003, 47.98196], [18.586952, 49.47831], [14.994042, 49.068268], [15.450293, 47.573086]]",
          "X-Amz-Meta-Mode": "IW",
          "X-Amz-Meta-Polarisation": "VV VH",
          "X-Amz-Meta-Producttype": "GRD"
        },
        {
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Uuid": "409a8258-4b72-4154-8211-60f86669ee90",
          "X-Amz-Meta-Producttype": "GRD",
          "X-Amz-Meta-Polarisation": "VV VH",
          "X-Amz-Meta-Productlevel": "L1",
          "X-Amz-Meta-Coordinates": "[[14.994017, 49.068356], [18.588295, 49.478531], [18.230318, 50.97448], [14.523001, 50.562672], [14.994017, 49.068356]]",
          "X-Amz-Meta-Mode": "IW",
          "X-Amz-Meta-Status": "Queued",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231104T164345_20231104T164410_051070_062883_7BA6",
          "content-type": "application/octet-stream",
          "X-Amz-Meta-Format": "SAFE"
        },
        {
          "X-Amz-Meta-Productlevel": "L1",
          "X-Amz-Meta-Mode": "IW",
          "X-Amz-Meta-Format": "SAFE",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231104T164410_20231104T164435_051070_062883_C660",
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Uuid": "d048ab9a-7e64-4ed2-9d22-703737a506aa",
          "X-Amz-Meta-Coordinates": "[[14.522973, 50.562763], [18.231846, 50.974712], [17.854832, 52.469223], [14.023267, 52.054729], [14.522973, 50.562763]]",
          "X-Amz-Meta-Polarisation": "VV VH",
          "X-Amz-Meta-Producttype": "GRD",
          "content-type": "application/octet-stream",
          "X-Amz-Meta-Status": "Queued"
        },
        {
          "X-Amz-Meta-Productlevel": "L1",
          "X-Amz-Meta-Status": "Queued",
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Polarisation": "VV VH",
          "X-Amz-Meta-Producttype": "GRD",
          "X-Amz-Meta-Mode": "IW",
          "content-type": "application/octet-stream",
          "X-Amz-Meta-Format": "SAFE",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231106T051747_20231106T051812_051092_062947_9B39",
          "X-Amz-Meta-Coordinates": "[[15.650861, 50.676769], [16.152039, 52.168552], [12.307861, 52.58337], [11.930406, 51.088985], [15.650861, 50.676769]]",
          "X-Amz-Meta-Uuid": "6d21e006-10d0-42f2-8aa6-f263a62e44a6"
        },
        {
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Coordinates": "[[15.179884, 49.18227], [15.650835, 50.676678], [11.931798, 51.088764], [11.574557, 49.592747], [15.179884, 49.18227]]",
          "X-Amz-Meta-Status": "Queued",
          "X-Amz-Meta-Format": "SAFE",
          "X-Amz-Meta-Polarisation": "VV VH",
          "X-Amz-Meta-Uuid": "6994c471-b542-4500-bdb0-9156d9d1aa48",
          "X-Amz-Meta-Producttype": "GRD",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231106T051812_20231106T051837_051092_062947_9207",
          "X-Amz-Meta-Productlevel": "L1",
          "X-Amz-Meta-Mode": "IW",
          "content-type": "application/octet-stream"
        },
        {
          "X-Amz-Meta-Coordinates": "[[14.718754, 47.68766], [15.179857, 49.182178], [11.575904, 49.592522], [11.219501, 48.096699], [14.718754, 47.68766]]",
          "content-type": "application/octet-stream",
          "X-Amz-Meta-Producttype": "GRD",
          "X-Amz-Meta-Format": "SAFE",
          "X-Amz-Meta-Mode": "IW",
          "X-Amz-Meta-Uuid": "abeeaeec-2b02-4bfa-a46a-16bea630848f",
          "X-Amz-Meta-Productlevel": "L1",
          "X-Amz-Meta-Size": "1.65 GB",
          "X-Amz-Meta-Identifier": "S1A_IW_GRDH_1SDV_20231106T051837_20231106T051902_051092_062947_201D",
          "X-Amz-Meta-Status": "Queued",
          "X-Amz-Meta-Polarisation": "VV VH"
        }
      ]
    };
  