const createEmptyProductDTO = () => {
    return {
        uuid: '',
        identifier: '',
        mode: '',
        status: '',
        polarisation: '',
        size: '',
        productLevel: '',
        productType: '',
        format: '',
        coordinates: '',
        date: ''
    }
}


export function swapCoordinates(coordinates) {
    return coordinates.map(pair => [pair[1], pair[0]]);
  }

export const mapGetAvailableProductsReponseToDto = (data) => {
    const result = [];
    data.products.forEach((product) => {
        const productDTO = createEmptyProductDTO();
        productDTO.uuid = product["X-Amz-Meta-Uuid"];
        productDTO.identifier = product["X-Amz-Meta-Identifier"];
        productDTO.mode = product["X-Amz-Meta-Mode"];
        productDTO.status = product["X-Amz-Meta-Status"];
        productDTO.polarisation = product["X-Amz-Meta-Polarisation"];
        productDTO.size = product["X-Amz-Meta-Size"];
        productDTO.productLevel = product["X-Amz-Meta-Productlevel"];
        productDTO.productType = product["X-Amz-Meta-Producttype"];
        productDTO.format = product["X-Amz-Meta-Format"];
        productDTO.date = product["X-Amz-Meta-Date"];
        productDTO.coordinates = swapCoordinates(JSON.parse(product["X-Amz-Meta-Coordinates"]));
        result.push(productDTO);
    });
    console.log(result);
    return result;
}

const parseProductsResponse = (jsonObject) => {
    if (jsonObject && Array.isArray(jsonObject.products)) {
      const updatedProducts = jsonObject.products.map(product => {
        const updatedProduct = {};
          Object.keys(product).forEach(key => {
          let newKey = key;
          if (key.startsWith('X-Amz-Meta-')) {
            newKey = key.substring('X-Amz-Meta-'.length);
          }
          updatedProduct[newKey] = product[key];
        });
  
        updatedProduct.Coordinates = swapCoordinates(JSON.parse(updatedProduct.Coordinates));
        
        return updatedProduct;
      });
      return {
        ...jsonObject,
        products: updatedProducts
      };
    }
    return jsonObject;
  }
  