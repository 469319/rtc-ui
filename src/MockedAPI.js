import { mapGetAvailableProductsReponseToDto, mapPostProductResponseToDto, mapDownloadProductResponseToDto } from "./Mapper";
import { availableProductsResponse, searchProductsResponse } from "./MockData";

class MockedAPI {

  // static getAvailableProducts = async () => {
  //   return new Promise(resolve => {
  //     setTimeout(() => {
  //       resolve(mapGetAvailableProductsReponseToDto(availableProductsResponse));
  //     }, 500);
  //   });
  // }

  static getAvailableProducts = async () => {
    try {
      const response = await fetch('https://stg.rtc.afolab.cz/products');
      const data = await response.json();
      console.log(data);
      return data.products;
    } catch (error) {
      console.error("Failed to fetch products:", error);
      return [];
    }
  }

  // static getSearchProducts = async () => {
  //   return new Promise(resolve => {
  //       setTimeout(() => {
  //           resolve(mapGetAvailableProductsReponseToDto(searchProductsResponse));
  //           }, 500);
  //   });
  // }

  static getSearchProducts = async () => {
    try {
      const response = await fetch('https://stg.rtc.afolab.cz/search');
      const data = await response.json();
      console.log(data);
      return data.products;
    } catch (error) {
      console.error("Failed to fetch products:", error);
      return [];
    }
  }

  static postProduct = async (identifier) => {
    try {
      const response = await fetch(`https://stg.rtc.afolab.cz/product/${identifier}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        }
      });
    } catch (error) {
      console.error("Failed to post product:", error);
      return [];
    }
  }

  // static postProduct = async (product) => {
  //   return new Promise(resolve => {
  //     setTimeout(() => {
  //       resolve({});
  //     }, 500);
  //   });
  // }

  static downloadProduct = async (productId) => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve({});
      }, 500);
    });
  }

};

export default MockedAPI;
