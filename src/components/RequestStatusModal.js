import React, { useEffect, useState } from 'react';
import RtcApiService from '../services/RtcApiService';
import { ProductStatus } from '../constants/ProductStatus';
import Button from '@mui/joy/Button';
import Divider from '@mui/joy/Divider';
import DialogTitle from '@mui/joy/DialogTitle';
import DialogContent from '@mui/joy/DialogContent';
import DialogActions from '@mui/joy/DialogActions';
import Modal from '@mui/joy/Modal';
import ModalDialog from '@mui/joy/ModalDialog';
import WarningRoundedIcon from '@mui/icons-material/WarningRounded';

const RequestStatusModal = ({ identifier, status, setShowAlert }) => {
  const [open, setOpen] = useState(false);
  const [label, setLabel] = useState('');
  const [dialogText, setDialogText] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [action, setAction] = useState(() => () => {});

  return (
    <React.Fragment>
      <Button disabled={isDisabled} onClick={showModal ? () => setOpen(true) : action} variant="solid" size="md" color="primary" aria-label={label} sx={{ ml: 'auto', alignSelf: 'center', fontWeight: 600, height: 4 }}>
        {label}
      </Button>
      <Modal open={open} onClose={() => setOpen(false)}>
        <ModalDialog variant="outlined" role="alertdialog">
          <DialogTitle>
            <WarningRoundedIcon />
            Confirmation
          </DialogTitle>
          <Divider />
          <DialogContent>
            {dialogText}
          </DialogContent>
          <DialogActions>
            <Button variant="solid" color="primary" onClick={() => { action(); setOpen(false); }}>
              Confirm
            </Button>
            <Button variant="plain" color="neutral" onClick={() => setOpen(false)}>
              Cancel
            </Button>
          </DialogActions>
        </ModalDialog>
      </Modal>
    </React.Fragment>
  );
};

export default RequestStatusModal;
