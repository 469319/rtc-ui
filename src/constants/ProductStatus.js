export const ProductStatus = Object.freeze({
    NOT_PROCESSED: "Not Processed",
    ERROR: "Error",
    QUEUED: "Queued",
    IN_PROGRESS: "In Progress",
    FINISHED: "Finished",
});